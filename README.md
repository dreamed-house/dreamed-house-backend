# Dreamed House Project - Backend

## Description

A backend project for Software Verification and Validation class in University

---

This project is for users to create house proformas. Users will have to log in, after registering, to be able to save and view their proformas

---

## Technologies

* C#
* ASP.NET Core
* Entity Framework Core
* MySQL
* JWT
